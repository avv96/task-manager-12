package ru.tsc.vinokurov.tm.service;

import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.api.service.ICommandService;
import ru.tsc.vinokurov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

}
