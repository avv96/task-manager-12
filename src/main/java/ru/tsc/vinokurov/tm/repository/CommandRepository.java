package ru.tsc.vinokurov.tm.repository;

import ru.tsc.vinokurov.tm.constant.ArgumentConst;
import ru.tsc.vinokurov.tm.constant.TerminalConst;
import ru.tsc.vinokurov.tm.model.Command;
import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system info."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version"
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of terminal commands."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close Application."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display commands list."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display arguments list."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Clear list of tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Display list of tasks."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Remove task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Show task by id"
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Show task by index."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Clear list of projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Display list of projects."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Remove project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Show project by id"
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index."
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Change status of task by id."
    );
    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change status of task by index."
    );
    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id."
    );
    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index."
    );
    private static final Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete task by id."
    );
    private static final Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index."
    );
    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change status of project by id."
    );
    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change status of project by index."
    );
    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id."
    );
    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index."
    );
    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id."
    );
    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            ARGUMENTS, COMMANDS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            EXIT
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }

}
