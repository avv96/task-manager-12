package ru.tsc.vinokurov.tm.controller;

import ru.tsc.vinokurov.tm.api.controller.ITaskController;
import ru.tsc.vinokurov.tm.api.service.ITaskService;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    final private ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> taskList = taskService.findAll();
        int index = 1;
        for (final Task task: taskList) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void showTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        Status status = task.getStatus();
        if (status != null) System.out.println("STATUS: " + task.getStatus());
    }

    @Override
    public void changeStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        if (status == null) {
            System.out.println("FAIL");
            return;
        }
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER STATUS: ");
        System.out.print(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        if (status == null) {
            System.out.println("FAIL");
            return;
        }
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Status status = Status.IN_PROGRESS;
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Status status = Status.IN_PROGRESS;
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Status status = Status.COMPLETED;
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Status status = Status.COMPLETED;
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        System.out.println("OK");
    }

}
